<?php

/*
 * This file is part of the Claroline Connect package.
 *
 * (c) Claroline Consortium <consortium@claroline.net>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Claroline\CoreBundle\Manager;

use Claroline\CoreBundle\Entity\User;
use Claroline\CoreBundle\Library\Configuration\PlatformConfigurationHandler;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @DI\Service("claroline.manager.locale_manager")
 */
class LocaleManager
{
    private $defaultLocale;
    private $finder;
    private $locales;
    private $userManager;
    private $tokenStorage;
    private $configHandler;

    /**
     * LocaleManager constructor.
     *
     * @DI\InjectParams({
     *     "configHandler" = @DI\Inject("claroline.config.platform_config_handler"),
     *     "userManager"   = @DI\Inject("claroline.manager.user_manager"),
     *     "tokenStorage"  = @DI\Inject("security.token_storage")
     * })
     *
     * @param PlatformConfigurationHandler $configHandler
     * @param UserManager                  $userManager
     * @param TokenStorageInterface        $tokenStorage
     */
    public function __construct(
        PlatformConfigurationHandler $configHandler,
        UserManager $userManager,
        TokenStorageInterface $tokenStorage
    ) {
        $this->configHandler = $configHandler;
        $this->userManager = $userManager;
        $this->defaultLocale = $configHandler->getParameter('locale_language');
        $this->finder = new Finder();
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Get a list of available languages in the platform.
     *
     * @param string $path The path of translations files
     *
     * @return array
     */
    public function retrieveAvailableLocales($path = '/../Resources/translations/')
    {
        $locales = [];
        $data = $this->configHandler->getParameter('locales.available');
        foreach ($data as $locale) {
            $locales[$locale] = $locale;
        }

        return $locales;
    }

    public function getLocales()
    {
        $available = $this->getAvailableLocales();
        $implemented = array_keys($this->getImplementedLocales());

        return array_map(function ($locale) use ($available) {
            return [
                'name' => $locale,
                'enabled' => array_key_exists($locale, $available),
            ];
        }, $implemented);
    }

    /**
     * Get a list of available languages in the platform.
     *
     * @param string $path The path of translations files
     *
     * @return array
     */
    public function getImplementedLocales($path = '/../Resources/translations/')
    {
        $locales = [];
        $finder = $this->finder->files()->in(__DIR__.$path)->name('/platform\.[^.]*\.json/');

        foreach ($finder as $file) {
            $locale = str_replace(['platform.', '.json'], '', $file->getRelativePathname());
            $locales[$locale] = $locale;
        }

        return $locales;
    }

    /**
     * Get a list of available languages in the platform.
     *
     * @return array
     */
    public function getAvailableLocales()
    {
        if (!$this->locales) {
            $this->locales = $this->retrieveAvailableLocales();
        }

        return $this->locales;
    }

    /**
     * Set locale setting for current user if this locale is present in the platform.
     *
     * @param string $locale The locale string as en, fr, es, etc
     */
    public function setUserLocale($locale)
    {
        $this->userManager->setLocale($this->getCurrentUser(), $locale);
    }

    /**
     * This method returns the user locale and store it in session, if there is no user this method return default
     * language or the browser language if it is present in translations.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return string The locale string as en, fr, es, etc
     */
    public function getUserLocale(Request $request)
    {
        $locales = $this->getAvailableLocales();
        $preferred = explode('_', $request->getPreferredLanguage());

        if ($request->query->get('_locale')) {
            $locale = $request->query->get('_locale');
        } elseif ($request->attributes->get('_locale')) {
            $locale = $request->attributes->get('_locale');
        } elseif (($user = $this->getCurrentUser()) && $user->getLocale()) {
            $locale = $user->getLocale();
        } elseif ($request->getSession() && ($sessionLocale = $request->getSession()->get('_locale'))) {
            $locale = $sessionLocale;
        } elseif (count($preferred) > 0 && isset($locales[$preferred[0]])) {
            $locale = $preferred[0];
        } else {
            $locale = $this->defaultLocale;
        }

        if ($session = $request->getSession()) {
            $session->set('_locale', $locale);
        }

        return $locale;
    }

    /**
     * Get Current User.
     *
     * @return User|null
     */
    private function getCurrentUser()
    {
        if (is_object($token = $this->tokenStorage->getToken()) && is_object($user = $token->getUser())) {
            return $user;
        }

        return null;
    }
}
