<?php

namespace Claroline\CoreBundle\Manager\Workspace\Transfer;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * @DI\Service("claroline.transfer.rich_text")
 */
class RichText
{
    public function update()
    {
    }

    public function addResource()
    {
    }
}
