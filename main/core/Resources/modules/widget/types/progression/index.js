import {ProgressionWidget} from '#/main/core/widget/types/progression/containers/widget'
import {ProgressionWidgetParameters} from '#/main/core/widget/types/progression/components/parameters'

export const Parameters = () => ({
  component: ProgressionWidgetParameters
})

/**
 * Progression widget application.
 */
export const App = () => ({
  component: ProgressionWidget
})
