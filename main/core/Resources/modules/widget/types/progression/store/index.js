import {actions} from '#/main/core/widget/types/progression/store/actions'
import {reducer} from '#/main/core/widget/types/progression/store/reducer'
import {selectors} from '#/main/core/widget/types/progression/store/selectors'

export {
  actions,
  reducer,
  selectors
}
