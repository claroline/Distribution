
import {actions} from '#/main/core/header/workspaces/store/actions'
import {reducer} from '#/main/core/header/workspaces/store/reducer'
import {selectors} from '#/main/core/header/workspaces/store/selectors'

export {
  actions,
  reducer,
  selectors
}
