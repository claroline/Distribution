import {WorkspacesMenu} from '#/main/core/header/workspaces/containers/menu'

// expose main component to be used by the header
export default WorkspacesMenu
