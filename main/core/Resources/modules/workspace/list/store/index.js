
import {actions} from '#/main/core/workspace/list/store/actions'
import {reducer} from '#/main/core/workspace/list/store/reducer'

export {
  actions,
  reducer
}
