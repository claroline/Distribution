
import {actions} from '#/main/core/workspace/creation/store/actions'
import {reducer} from '#/main/core/workspace/creation/store/reducer'

export {
  actions,
  reducer
}
