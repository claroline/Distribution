
import {actions} from '#/main/core/administration/parameters/main/store/actions'
import {reducer} from '#/main/core/administration/parameters/main/store/reducer'
import {selectors} from '#/main/core/administration/parameters/main/store/selectors'

export {
  actions,
  reducer,
  selectors
}
