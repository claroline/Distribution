
import {reducer} from '#/main/core/administration/parameters/technical/store/reducer'
import {selectors} from '#/main/core/administration/parameters/technical/selectors'

export {
  reducer,
  selectors
}
