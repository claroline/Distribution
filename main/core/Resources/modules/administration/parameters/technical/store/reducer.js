import {makeFormReducer} from '#/main/app/content/form/store/reducer'

const reducer = {
  parameters: makeFormReducer('parameters')
}

export {
  reducer
}
