import {reducer} from '#/main/core/administration/parameters/appearance/store/reducer'
import {selectors} from '#/main/core/administration/parameters/appearance/store/selectors'

export {
  selectors,
  reducer
}
