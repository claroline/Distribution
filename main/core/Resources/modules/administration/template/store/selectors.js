const locales = (state) => state.locales
const defaultLocale = (state) => state.defaultLocale

export const selectors = {
  locales,
  defaultLocale
}
