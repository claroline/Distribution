/**
 * Trash tool store.
 */

import {reducer} from '#/main/core/tools/trash/store/reducer'
import {actions} from '#/main/core/tools/trash/store/actions'

export {
  reducer,
  actions
}
