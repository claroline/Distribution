const items = (state) => state.items
const levelMax = (state) => state.levelMax

export const selectors = {
  items,
  levelMax
}
