import {makeReducer} from '#/main/app/store/reducer'

const reducer = {
  items: makeReducer([])
}

export {reducer}