/**
 * Contact store.
 */

import {actions} from '#/main/core/tools/contact/store/actions'
import {reducer} from '#/main/core/tools/contact/store/reducer'

export {
  actions,
  reducer
}
