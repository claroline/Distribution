/**
 * Desktop parameters store.
 */

import {reducer} from '#/main/core/tools/desktop-parameters/store/reducer'
import {selectors} from '#/main/core/tools/desktop-parameters/store/selectors'

export {
  reducer,
  selectors
}
