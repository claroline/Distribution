
import {hasPermission} from '#/main/app/security/permissions'
import {
  currentUser,
  isAdmin,
  isAuthenticated
} from '#/main/app/security/authenticated'

export {
  currentUser,
  isAdmin,
  isAuthenticated,
  hasPermission
}
