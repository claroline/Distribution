import {LtiResource} from '#/plugin/lti/resources/lti/containers/resource'

/**
 * LTI resource application.
 *
 * @constructor
 */
export const App = () => ({
  component: LtiResource
})