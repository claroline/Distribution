import {actions} from '#/plugin/lti/administration/lti/store/actions'
import {reducer} from '#/plugin/lti/administration/lti/store/reducer'

export {
  actions,
  reducer
}
