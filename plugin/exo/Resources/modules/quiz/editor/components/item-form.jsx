import React from 'react'
import {PropTypes as T} from 'prop-types'
import get from 'lodash/get'

import {trans, tex} from '#/main/app/intl/translation'
import {HINT_ADD, HINT_CHANGE, HINT_REMOVE} from './../actions'
import {FormGroup} from '#/main/app/content/form/components/group'
import {HtmlGroup} from '#/main/core/layout/form/components/group/html-group'
import {TextGroup} from '#/main/core/layout/form/components/group/text-group'
import {Textarea} from '#/main/core/layout/form/components/field/textarea'
import {ToggleableSet} from '#/main/core/layout/form/components/fieldset/toggleable-set'
import {Button} from '#/main/app/action/components/button'
import {CALLBACK_BUTTON} from '#/main/app/buttons'
import ObjectsEditor from './item-objects-editor'
import TagsEditor from '#/plugin/tag/item-tags-editor'
import {CheckGroup} from '#/main/core/layout/form/components/group/check-group'

// TODO: add categories, define-as-model

const Metadata = props =>
  <div>
    <TextGroup
      id={`item-${props.item.id}-title`}
      label={trans('title')}
      value={props.item.title}
      onChange={text => props.onChange('title', text)}
    />

    <HtmlGroup
      id={`item-${props.item.id}-description`}
      label={trans('description')}
      value={props.item.description}
      onChange={text => props.onChange('description', text)}
    />

    {props.item.rights.edit &&
      <CheckGroup
        id={`item-${props.item.id}-editable`}
        label={tex('protect_update')}
        value={props.item.meta.protectQuestion}
        onChange={checked => props.onChange('meta.protectQuestion', checked)}
      />
    }

    <CheckGroup
      id={`item-${props.item.id}-mandatory`}
      label={props.mandatoryQuestions ? tex('make_optional'): tex('mandatory_answer')}
      value={props.item.meta.mandatory}
      onChange={checked => props.onChange('meta.mandatory', checked)}
    />

    <FormGroup
      id={`item-${props.item.id}-objects`}
      label={tex('question_objects')}
    >
      <ObjectsEditor
        showModal={props.showModal}
        validating={props.validating}
        item={props.item}
      />
    </FormGroup>

    <FormGroup
      id={`item-${props.item.id}-tags`}
      label={trans('tags')}
    >
      <TagsEditor
        item={props.item}
      />
    </FormGroup>
  </div>

Metadata.propTypes = {
  item: T.shape({
    id: T.string.isRequired,
    title: T.string.isRequired,
    description: T.string.isRequired,
    rights: T.object.isRequired,
    meta: T.shape({
      mandatory: T.bool.isRequired,
      protectQuestion: T.bool.isRequired
    }).isRequired
  }).isRequired,
  mandatoryQuestions: T.bool.isRequired,
  showModal: T.func.isRequired,
  onChange: T.func.isRequired,
  validating: T.bool.isRequired
}

const Hint = props =>
  <div className="hint-item">
    <div className="hint-value">
      <Textarea
        id={`hint-${props.id}`}
        value={props.value}
        onChange={value => props.onChange(HINT_CHANGE, {id: props.id, value})}
      />
    </div>

    <input
      id={`hint-${props.id}-penalty`}
      title={tex('penalty')}
      type="number"
      min="0"
      value={props.penalty}
      className="form-control hint-penalty"
      aria-label={tex('penalty')}
      onChange={e => props.onChange(
        HINT_CHANGE,
        {id: props.id, penalty: e.target.value}
      )}
    />

    <Button
      id={`hint-${props.id}-delete`}
      className="btn-link"
      type={CALLBACK_BUTTON}
      icon="fa fa-fw fa-trash-o"
      label={trans('delete', {}, 'actions')}
      callback={props.onRemove}
    />
  </div>

Hint.propTypes = {
  id: T.string.isRequired,
  value: T.string.isRequired,
  penalty: T.number.isRequired,
  onChange: T.func.isRequired,
  onRemove: T.func.isRequired
}

const Hints = props =>
  <div className="hint-items">
    <label className="control-label" htmlFor="hint-list">
      {tex('hints')}
    </label>

    {props.hints.length === 0 &&
      <div className="no-hint-info">{tex('no_hint_info')}</div>
    }

    {props.hints.length !== 0 &&
      <ul id="hint-list">
        {props.hints.map(hint =>
          <li key={hint.id}>
            <Hint
              {...hint}
              onChange={props.onChange}
              onRemove={() => props.onChange(HINT_REMOVE, {id: hint.id})}
            />
          </li>
        )}
      </ul>
    }

    <button
      type="button"
      className="btn btn-block btn-default"
      onClick={() => props.onChange(HINT_ADD, {})}
    >
      <span className="fa fa-fw fa-plus"/>
      {tex('add_hint')}
    </button>
  </div>

Hints.propTypes = {
  hints: T.arrayOf(T.shape({
    id: T.string.isRequired
  })).isRequired,
  onChange: T.func.isRequired
}

const ItemForm = props =>
  <fieldset>
    <HtmlGroup
      id={`item-${props.item.id}-content`}
      label={tex('question')}
      value={props.item.content}
      onChange={content => props.onChange('content', content)}
      warnOnly={!props.validating}
      error={get(props.item, '_errors.content')}
    />

    <ToggleableSet
      showText={tex('show_metadata_fields')}
      hideText={tex('hide_metadata_fields')}
    >
      <Metadata
        mandatoryQuestions={props.mandatoryQuestions}
        item={props.item}
        showModal={props.showModal}
        onChange={props.onChange}
        validating={props.validating}
      />
    </ToggleableSet>

    <hr className="item-content-separator" />

    {props.children}

    <hr className="item-content-separator" />

    <ToggleableSet
      showText={tex('show_interact_fields')}
      hideText={tex('hide_interact_fields')}
    >
      <Hints
        hints={props.item.hints}
        onChange={props.onHintsChange}
      />

      <hr className="item-content-separator" />

      <FormGroup
        id={`item-${props.item.id}-feedback`}
        label={tex('feedback')}
      >
        <Textarea
          id={`item-${props.item.id}-feedback`}
          value={props.item.feedback}
          onChange={text => props.onChange('feedback', text)}
        />
      </FormGroup>
    </ToggleableSet>
  </fieldset>

ItemForm.propTypes = {
  item: T.shape({
    id: T.string.isRequired,
    content: T.string.isRequired,
    hints: T.arrayOf(T.object).isRequired,
    feedback: T.string.isRequired,
    _errors: T.object
  }).isRequired,
  mandatoryQuestions: T.bool.isRequired,
  children: T.element.isRequired,
  validating: T.bool.isRequired,
  showModal: T.func.isRequired,
  onChange: T.func.isRequired,
  onHintsChange: T.func.isRequired
}

export {
  ItemForm
}
