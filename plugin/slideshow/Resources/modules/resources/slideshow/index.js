
import {SlideshowResource} from '#/plugin/slideshow/resources/slideshow/containers/resource'

/**
 * Slideshow resource application.
 *
 * @constructor
 */
export const App = () => ({
  component: SlideshowResource
})
