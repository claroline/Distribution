import {WikiResource} from '#/plugin/wiki/resources/wiki/containers/resource'

/**
 * Wiki resource application.
 *
 * @constructor
 */
export const App = () => ({
  component: WikiResource
})
