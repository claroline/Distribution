/**
 * Entry store.
 */

import {actions} from '#/plugin/claco-form/resources/claco-form/player/store/actions'
import {reducer} from '#/plugin/claco-form/resources/claco-form/player/store/reducer'

export {
  actions,
  reducer
}
