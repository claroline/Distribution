
import {actions} from '#/plugin/tag/administration/tags/store/actions'
import {selectors} from '#/plugin/tag/administration/tags/store/selectors'
import {reducer} from '#/plugin/tag/administration/tags/store/reducer'

export {
  actions,
  selectors,
  reducer
}
